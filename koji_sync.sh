#!/bin/bash

#set -e

# Simple for-loop + wget job to retrieve all koji packages
# Straight reposync keeps failing w/ checksum issues

URLS=`reposync -v --download-path=./ --urls  --repoid=RockyKoji | grep http`

for i in `echo "${URLS}"`; do
  RESULT=$(wget -S -N "${i}" 2>&1 >/dev/null)
  
  if [[ $? -ne "0" ]]; then
    ERRORS="${ERRORS}\n\nERROR :: ${i}\n${RESULT}"
  fi
done


echo -e "\n\n\n\nTHESE PACKAGES HAD DOWNLOAD ERRORS: \n${ERRORS}\n"
