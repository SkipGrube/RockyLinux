#!/usr/bin/bash

# Scan through a directory and subdirectories of RPM builds, and see if they match 
# names in a corresponding yum/dnf repo.

# This allows us to figure out what RPMs need to be put in the "official" repo, and which ones 
# should go in to our "Devel" repo (that are just used for builds)


# Syntax is: 
# match_packages ./build_folder  name_of_repo
# Ex: ./match_packages.sh  ./BaseOS   baseos
#

# TYPICAL USAGE EXAMPLE ::
# Get all BaseOS RPMs into the official repo, and all other build artifacts into the devel repo:
#
#
# ~/src/RockyLinux/match_packages.sh ./BaseOS/ baseos > /tmp/matched_basos_official.txt         # We want baseos official packages to match ONLY what's in baseos
# ~/src/RockyLinux/match_packages.sh ./BaseOS/ baseos,appstream,powertools,extras,plus > /tmp/matched_baseos_devel.txt   # We match devel packages against everything, b/c a -devel package could exist in another repo
#
# Filter output, and copy package to appropriate repo:
# grep -v 'NOT_IN_REPO\:' /tmp/matched_baseos_official.txt   | awk '{print $2}' | xargs -n 1 -t -I {} cp -n {} /var/www/html/RockyDevel/BaseOS_final/
# grep 'NOT_IN_REPO' /tmp/matched_baseos_devel.txt  | awk '{print $2}' | xargs -n 1 -t -I {} cp -n {} /var/www/html/RockyDevel/BaseOS_devel/
#
# Finally, if you want a list of "new" packages the most recent build produced,
# simply 'ls *.rpm' that repo, sort by date.  Find the ones that were just copied in, those are the new/added ones (cp -n means no overwrite happens)
#


FOLDER=$1
REPO=$2


# Need to build a list of all the packages in a repo, including not-enabled modules:
# 
# we do it via reposync with just the URLS, and isolate the filename after the last "/" in the url (and strip off the .rpm)
_pkglist=`reposync -u --repoid="${REPO}" | grep '\.rpm'  | sed 's/.*\(.*\)\//\1/' | sed 's/\.rpm//'`


REPO_LIST=""

for i in $_pkglist; do 
  # arch is the last field in the line .noarch , .i686, etc.)
  _arch=`echo "$i" | awk -F "." '{print $NF}'`

  # isolate the rpm name by finding the first occurrence of -# (hyphen-number).  That should be the start of the version string
  _name=`echo "$i" | sed 's/-[0-9].*//'`

  REPO_LIST="${REPO_LIST}
${_name}.${_arch}"

done


# Now, REPO_LIST is a list of all rpms in the given repo(s)
# in the format "%{name}.%{arch}"



# Loop through every rpm (minus debug and source rpms) within the folder given as parameter
# Figure out whether it is IN_REPO or NOT_IN_REPO
for file in `find ${FOLDER} -iname "*.rpm" | grep -v '\.src\.rpm' | grep -v '\-debuginfo\-' | grep -v '\-debugsource\-'`; do

  # proper name of the rpm, in same format as REPO_LIST
  _name=`rpm -qp --queryformat "%{NAME}.%{ARCH}\n" "${file}"`
  
  
  _match=`echo "${REPO_LIST}" | grep -x "${_name}" | wc -l`
  
  #echo "RPM == $_name"

  if [[ "${_match}" -gt 0 ]]; then 
    echo "IN_REPO:    ${file}"
  
  elif [[ "${_match}" -eq 0 ]]; then
    echo "NOT_IN_REPO:    ${file}"
  
  else 
    echo "ERROR:  SOMETHING WRONG.  Multiple matches found :: ${_match} .  This shouldn't happen....  (rpm == $_name ) "
    
  fi

done 
