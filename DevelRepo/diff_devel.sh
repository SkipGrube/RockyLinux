#!/bin/bash

repodiff \
-a noarch,x86_64,i686  --simple  --downgrade \
-o Rocky8.3_BaseOS \
-o Rocky8.3_AppStream \
-o Rocky8.3_PowerTools \
-o Rocky8.3_HighAvailability \
-o Rocky8.3_ResilientStorage \
-o Rocky8.4rc1_BaseOS \
-o Rocky8.4rc1_AppStream \
-o Rocky8.4rc1_PowerTools \
-o Rocky8.4rc1_HighAvailability \
-o Rocky8.4rc1_ResilientStorage \
-o Rocky8.4_BaseOS \
-o Rocky8.4_AppStream \
-o Rocky8.4_PowerTools \
-o Rocky8.4_HighAvailability \
-o Rocky8.4_ResilientStorage \
-o Rocky8.4_plus \
-o Rocky8.4_extras \
-n RockyKoji \
-n RockyKoji_84
