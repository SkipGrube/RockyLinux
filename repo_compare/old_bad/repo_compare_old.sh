#!/bin/bash

# Simple script to do a Requires compare across packages from 2 different (but almost identical) repos

# Takes a repo, a repo to compare it to, and walks through all packages.  Find matches in name/version and compare requires


REPO1=$1
REPO2=$2
#FOLDER=$3

mkdir -p "${REPO1}"  "${REPO2}"



IFS='
'

for pkg in `ls -1 ${REPO1}/*.txt`; do
  
  _file=`basename ${pkg}`
  
  if [ ! -f ${REPO2}/${_file} ]; then
    echo "NOT FOUND :: Package  ${_file} exists in REPO1 ( ${REPO1} ) , but not in REPO2 ( ${REPO2} )"
    continue
  fi
  
  pkgdiff=`git diff --no-index ${REPO1}/${_file}   ${REPO2}/${_file}`

  if [[ -z ${pkgdiff} ]]; then
    echo "IDENTICAL_OK :: ${_file}"
  else
    echo -e "\nNOT IDENTICAL :: ${_file}\n${pkgdiff}\n\n"
  fi

done

