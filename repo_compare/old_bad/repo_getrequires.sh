#!/bin/bash

# First in a pair of scripts that extracts metadata (name/version, requires) from every package in 2 similar repos 
# (example:  Rocky 8 BaseOS and RHEL 8 BaseOS)
#
# This first script extracts the data via repoquery.  
# The other one, repo_compare.sh, produces a list of differing packages once the metadata is put into local files
#

REPO1=$1
REPO2=$2

mkdir -p "${REPO1}"  "${REPO2}"

packages=`dnf repoquery --repo ${REPO1} --queryformat "%{name}" | sort | uniq | grep -v "Updating Subscription Manager"`


IFS='
'


# The strategy is to query each package from each of the repos, and produce a .txt file per package with the version and requires info
# Each repo gets its own folder of .txt files.
#
# The files (named after the package) can then be compared with simple tools like diff, head, etc.
#

(
for pkg in `echo "${packages}"`; do
  
  # Unfortunately, RHEL includes a line about "Updating Subscription info" in STDOUT that we have to filter out.  Hacky, but needs to be done:
  echo "( ${REPO1} ) Now processing $pkg ..."
  dnf repoquery --latest-limit 1 --repo ${REPO1} --queryformat "%{name}-%{version}-%{release}\n%{requires}" ${pkg}  |  grep -v "Updating Subscription"  > ${REPO1}/${pkg}.txt

done
) & 


(
for pkg2 in `echo "${packages}"`; do
  
  echo "( ${REPO2} ) Now processing ${pkg2} ..."
  dnf repoquery --latest-limit 1 --repo ${REPO2} --queryformat "%{name}-%{version}-%{release}\n%{requires}" ${pkg2}  |  grep -v "Updating Subscription" > ${REPO2}/${pkg2}.txt
  
done
) &

wait

# Strip out module specific version info, because the git commit hashes will never match
# We are just doing the best we can by comparing simple version numbers
sed -i  -e 's/\.module.*$/\.module/g'  ${REPO1}/*.txt
sed -i  -e 's/\.module.*$/\.module/g'  ${REPO2}/*.txt


# Strip out ending tags from packages and requires ".el*" (ex: .el8_4 , .el8, .el8_3, etc.)
# They often differ based on build time / tagging, and we are only interested in software version compatibility
# We do this because we still want to keep the %RELEASE tag intact in the name/version string
sed -i  -e 's/\.el.*$//g'  ${REPO1}/*.txt
sed -i  -e 's/\.el.*$//g'  ${REPO2}/*.txt

