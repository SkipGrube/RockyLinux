#!/bin/bash

# Take raw list of packages and find ones that have no-longer missing deps. after 1 mock build run


#BUILD_FILE="$1"
#DIR=$2


# Should give the build problems table from the wiki (markdown table) as $1
# Give the directory to search with $2

builds=`grep -i 'missing dependency' $1 | sed 's/Missing dependency\://'`

IFS='
'
for i in `echo "$builds"`; do

  pkg=`echo "$i" | tr -d ' ' |  awk -F '|' '{print $2}'`
  deps=`echo "$i" | tr -d ',' | awk -F '|' '{print $3}' | tr -d '>' | tr -d '='`
  
  echo ""
  echo "pkg == ${pkg}"
  
  pkgOK=1

  for j in `echo "$deps" | tr ' ' '\n'`; do
    #echo "dep is == $j"

    # Bail out if we find "(" - we're not checking those
    if [[ $j == *"("* ]]; then
      pkgOK=0
      continue
    fi
	  
    # Skip to next one if this is a version number
    if [[ $j == *"."* ]]; then
      continue
    fi
	
    # Look for this req - mark pkg not OK if it's not found
    _rpms=`find $2 | grep $j | grep '\.rpm'`
    cnt=`echo "$_rpms" | wc -l`

    if [[ $cnt -eq 0 ]]; then
      pkgOK=0
    fi
    

  done
  
  if [[ $pkgOK -eq 1 ]]; then
    echo "$_rpms"
  fi
  #echo "pkg == ${pkg}        deps == ${deps}"
  

done
