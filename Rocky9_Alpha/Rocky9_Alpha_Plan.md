# Skip's Rocky 9 Alpha Bootstrap Plan
##################################


## Phase 1: Build RHEL 9 Beta, Straight-Up

- Get SRPM sources from ftp.redhat.com (BaseOS, AppStream, CRB)
- Build all sources using Mock + Lazybuilder, across several build loops to satisfy dependencies
- Iterate through several loops (6-8?) to build everything possible from RHEL9 beta alone
- Identify "hidden dependencies" for remaining error'd packages: build these as well
- Mock config points to RHEL 9 beta as currently published to fulfill dependencies
- After all SRPMs and dependencies are built, we have produced the **PHASE 1 BOOTSTRAP REPO**


<br />



## Phase 2: Build Rocky 9 Packages from Phase 1 Repo

- Rebuild all source packages in RHEL 9, just like Phase 1
- Sources now come from Rocky Git instead of SRPMs from Red Hat - debranding is (mostly) complete
- Mock config now uses Phase 1 bootstrap repo.  No more pointing to CentOS 9 Stream for dependencies.  We become "self-building"
- Should go quickly, because the phase 1 bootstrap should contain all necessary dependencies.  One pass should do it.
- This produces **PHASE 2: Rocky Pre-Alpha Repo**



<br />



## Phase 3: Build and/or Compose Standard Repos

- Rocky pre-alpha repo from phase 2 is a giant "clump" of packages
- Take this clump, and separate out the packages into the necessary repositories:  BaseOS, AppStream, CRB, etc.
- Use comps XML or RHEL 9 repoquery to figure out repo compositions
- Bonus: Use comps to also produce dnf groupinstall information
- Bonus: Sign all packages with a Rocky Beta/Alpha(?) signing key



<br />



## Phase 4: Build Alpha Test Image

- Use appliance-creator and kickstart with our repos to make a minimal pre-installed image  (similar to Raspberry Pi or cloud images)
- Can't do ISO yet:  ISO + Anaconda is hard and involves Pungi/Koji or Peridot
- Need alpha/beta (RPM tags?) rocky-release and possible other rocky original rpms to supplement repos
- Distribute image internally (RelEng?  Test Team?) for testing + feedback



<br />



## Other / Stretch Goals:

- Repeat this entire process on different architectures.  Aarch64 is easy to get ahold of, but maybe PPC / s390x / ARM32?
- Begin bootstrapping the modules.  Either wait until they're committed by Red Hat, or have a go at the CentOS 9 Stream ones....?




 
