#!/bin/bash


LAZYBUILDER=/home/skip/rocky9/lazybuilder
RESULTS=/opt/build/lazybuild_results


ANSIBLE_VARS="
major=9
minor=0

mock_isolation=simple
mock_builder=rpmbuild

repo_path=${RESULTS}

bootstrap=true
generate_repo=true

source_mode=git

git_commit=r9-beta
git_commit_hash=r9-beta
git_source_url=https://rocky-linux-sources-staging.a1.rockylinux.org

enable_cleanup=false

mock_force_classic_rpmbuild=true
"

#enable_cleanup=false
#git_source_url=https://rocky-linux-sources-staging.a1.rockylinux.org
# mock_srpm_path=
# source_name=

ANSIBLE_VARS=`echo ${ANSIBLE_VARS}  |  tr '\n' ' '`



IFS='
'
#for file in `ls -1 ~/rhel9_src/hidden_deps/*.rpm; ls -1 ~/rhel9_src/baseos/*.rpm; ls -1 ~/rhel9_src/appstream/*.rpm;  ls -1 ~/rhel9_src/codeready/*.rpm;  ls -1 ~/rhel9_src/extras/*.rpm`; do
#for file in `ls -1 ~/rhel9_src/baseos/bash-5*.rpm`; do
for pkg in `cat rhel9_pkgs.txt`; do
  #rpm_name=`rpm -qp --queryformat '%{NAME}' ${file} 2> /dev/null`
   
  
  # Let's build the package:
  cd ${LAZYBUILDER};  ansible-playbook   --extra-vars="${ANSIBLE_VARS}  git_url=https://git.rockylinux.org/staging/rpms/${pkg}  source_name=${pkg}"  --extra-vars="@lazybuild_vars.yml"  lazy_mock.yml
  
  
  
  
  

done
