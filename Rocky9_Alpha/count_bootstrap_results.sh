#!/bin/bash

# Simple script for generating counts of a bootstrap build pass.  How many packages we attempted, how many succeeded/failed,
# and *why* they failed (missing dependency, or other issue)

# Need to change the RESULTS and REPO vars at the top as we proceed through different bootstrap phases (and different folders)

# Mock results folder location (listing all the packages) and bootstrap repo folder location:
RESULTS="$1"
#REPO="lazybuild_results_deps/9-lazystrap-x86_64"




# Counters for our 2 possible error conditions: simple missing dependencies, or something more serious
MISSING_DEPS=0
OTHER_ERROR=0

TOTAL_PKGS=0
FAILED_PKGS=0
SUCCESS_PKGS=0

# Loop through our packages to find failed ones, and figure out why it failed: missing dependencies or another reason
for pkg in `ls -1 ${RESULTS}/`; do 
  
  # Skip this one if we don't have an EL9 or Fedora package available: (it's likely el8 or another version)
  testpkg=`ls -1 ${RESULTS}/${pkg}/*\.el9* ${RESULTS}/${pkg}/*\+el9*  ${RESULTS}/${pkg}/*\.fc* 2> /dev/null | wc -l`
  if [[ "${testpkg}" -eq "0" ]]; then
    continue
  fi

  # Confirm the package is el9 or fc34:
  let TOTAL_PKGS=${TOTAL_PKGS}+1



  failed=`find ${RESULTS}/${pkg}/*\.el9* ${RESULTS}/${pkg}/*\+el9*  ${RESULTS}/${pkg}/*\.fc*  -iname failed  2> /dev/null`
  
  # If the package did not fail, then check the next one, we don't need this one
  # Otherwise, increment failed pkg count
  if [[ `echo -n "$failed" | wc -m` -lt "2" ]]; then
    continue
  else
    let FAILED_PKGS=${FAILED_PKGS}+1
  fi
  
  # We search for the "No matching package to install" text:  this indicates a simple missing dependency from root.log
  # If the package failed and this isn't present, it indicates something more serious, like a compiler or testrun error
  missing=`grep -ri "No matching package to install"   ${RESULTS}/${pkg}/*\.el9* ${RESULTS}/${pkg}/*\+el9*  ${RESULTS}/${pkg}/*\.fc*   2> /dev/null`
  if [[ `echo "$missing" | wc -m` -gt "10" ]]; then 
    let MISSING_DEPS=${MISSING_DEPS}+1
  else
    let OTHER_ERROR=${OTHER_ERROR}+1
  fi
done




# If it didn't fail, then it succeeded.  How many?
let SUCCESS_PKGS=${TOTAL_PKGS}-${FAILED_PKGS}


# Output results:

echo "
Total SRPMs/Repos:                                 ${TOTAL_PKGS}

Completed Successful:                              ${SUCCESS_PKGS}


Failed Builds:                                     ${FAILED_PKGS}

Failed (Reason: missing dependencies)              ${MISSING_DEPS}
Failed (Reason: Other)                             ${OTHER_ERROR}

"


