#!/bin/bash

# Find error messages from failed builds


# Mock results folder location (listing all the packages) and bootstrap repo folder location:
RESULTS="$1"


# Package counts:  total packages, failed to build, and built successful
PKG_TOTAL=0
PKG_ERROR=0
PKG_GOOD=0


# Counts for types of error packages: missing deps and "other"
PKG_ERROR_OTHER=0
PKG_ERROR_MISS=0


# Loop through our packages to find failed ones, and figure out why it failed: missing dependencies or another reason
for pkg in `ls -1 ${RESULTS}/`; do 

  
  # If the package folder is completely blank, it's likely we didn't even get to the SRPM build stage.
  # If this happens, the below loop won't even happen, we have no versions!  Need to note that
  if [[ `ls -1 ${RESULTS}/${pkg} 2> /dev/null| grep -E '\+el9|\.el9|\.fc3' | sort -V |  tail -1 | wc -c`   -lt   "3" ]]; then
    let PKG_ERROR=${PKG_ERROR}+1
    let PKG_ERROR_OTHER=${PKG_ERROR_OTHER}+1
    ERROR_OUTPUT="${ERROR_OUTPUT}${pkg}    ( no version built ) :: \n"
    ERROR_OUTPUT="${ERROR_OUTPUT}Failed - Cannot build SRPM! (Issue with .spec/BuildSource) \n\n\n"

  fi

  
  for version in `ls -1 ${RESULTS}/${pkg} 2> /dev/null| grep -E '\+el9|\.el9|\.fc3' | sort -V |  tail -1`; do
    
    # Count package as part of total
    let PKG_TOTAL=${PKG_TOTAL}+1

    rpms=`ls -1 ${RESULTS}/${pkg}/$version/*/*.rpm 2> /dev/null`
 
    # If the package did not fail, then check the next one, we don't need this one (and count it as good)
    if [[ `echo -n "$rpms" | wc -m` -gt "5" ]]; then
      let PKG_GOOD=${PKG_GOOD}+1
      continue
    fi

    # If we make it here, the package failed - we need to count it
    let PKG_ERROR=${PKG_ERROR}+1


    ERROR_OUTPUT="${ERROR_OUTPUT}${pkg}    ( ${version} ) :: \n"
    # We search for the "No matching package to install" text:  this indicates a simple missing dependency from root.log
    # If the package failed and this isn't present, it indicates something more serious, like a compiler or testrun error
    missing=`grep -ri "No matching package to install" ${RESULTS}/${pkg}/${version}/ 2> /dev/null`
    if [[ `echo "${missing}" | wc -m` -gt "10" ]]; then 
      ERROR_OUTPUT="${ERROR_OUTPUT}${missing}\n\n\n"
      let PKG_ERROR_MISS=${PKG_ERROR_MISS}+1
    else
      ERROR_OUTPUT="${ERROR_OUTPUT}Failed - other reason\n\n\n"
      let PKG_ERROR_OTHER=${PKG_ERROR_OTHER}+1
    fi
  done
done


echo -e "
Total SRPMs/Repos:                                 ${PKG_TOTAL}

Completed Successful:                              ${PKG_GOOD}


Failed Builds:                                     ${PKG_ERROR}

Failed (Reason: missing dependencies)              ${PKG_ERROR_MISS}
Failed (Reason: Other)                             ${PKG_ERROR_OTHER}






${ERROR_OUTPUT}
"
