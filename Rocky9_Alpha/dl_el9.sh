#!/bin/bash

# Simple script to find the latest versions of RHEL9 packages and grab the binaries from the centos 9 stream koji

# Set the architecture you want in here with the "arch" variable (x86_64, aarch64, etc.)
# Specify your text file that is a simple list of git projects (source packages) as the first argument to this script

# Make sure you have space - it will attempt to download everything in the current directory!


# file with a newline-separated list of packages to get
pkgs=$1

arch=ppc64le

IFS='
'

for i in `cat $pkgs`; do
  
  
  # Get latest 9 name-version-release of package (ex: bash-5.1.8-4.el9)
  nvr=`GIT_TERMINAL_PROMPT=0  git ls-remote --tags https://git.rockylinux.org/staging/rpms/${i}.git  | grep r9-beta | grep -v '\^' | tail -1 | awk '{print $2}'  | awk -F '/' '{print $NF}'`
  
  # name is simply the name
  name=${i}
  
  # Version is the name removed, and the first text up until "-"
  version=`echo "${nvr}" | sed "s/${name}-//" | awk -F '-' '{print $1}'`
  
  # Release is what's left after name and version are removed
  release=`echo "${nvr}" | sed "s/${name}-${version}-//"`
  
  
  wget --mirror -np "https://kojihub.stream.centos.org/kojifiles/packages/${name}/${version}/${release}/${arch}/"
  wget --mirror -np "https://kojihub.stream.centos.org/kojifiles/packages/${name}/${version}/${release}/noarch/"

done
