#!/bin/bash


# Short and dirty parallel build script
# takes lists of srpms as input

# Ex: 
# ./mock_parallel.sh  srpmlist1  ~/srpmlist2  ~/list3


# Reads these lists and fires up mock in parallel - 1 thread per list
# You can build an arbitrary number of packages in parallel using this method - utilizing all processor cores

# I intend to do a bootstrap/test build of CentOS (and later RHEL)
# -Skip G.


# Generate RPM lists beforehand: (example)
# find /home/skip/build/sources/BaseOS-source/ -iname *.rpm | sort
#
# Skip's Strategy: 4 lists: BaseOS, Appstream1, Appstream2, Appstream3 (Each list ~600 packages)



# Folder where builds/results are stored:
BUILDS="/home/skip/build/bootstrap_results"
ARCH="x86_64"




count=0

# Random 3-character string to tag our chroot's with
#rnd=`head /dev/urandom | tr -dc A-Z | head -c3`
rnd="ABC"

# Loop through each argument which should be a big list of SRPMs
for file in `echo "$@"`; do
  
  # Keep track of which build thread we're starting, which we can label mock with "--uniqueext"
  let count=$count+1
  export count=${count}
  
  # Loop through the SRPMs in a given list, and have mock build them in turn
  # (Do it in a background sub-shell so we can start more threads of this)
  (
  for srpm in `cat "${file}"`; do
    _name=`rpm -qp --queryformat "%{NAME}"  ${srpm}`
		_fullname=`rpm -qp --queryformat '%{NAME}-%{VERSION}-%{RELEASE}' ${srpm}`
		
		_mock_dir=${BUILDS}/${folder}/${_name}/${_fullname}/${ARCH}
    mkdir -p ${_mock_dir}


    # Only build in a folder we haven't already built in
    rpmcount=`ls -1  "${_mock_dir}/*.rpm"   2> /dev/null | wc -l`

    if [[ "$rpmcount" == "0" ]]; then
      rm -f "${_mock_dir}/*.log" "${_mock_dir}/failed"
      time mock -v --isolation=simple  --uniqueext="${rnd}_${count}"  --resultdir=${_mock_dir}  $srpm
    else
      echo "FOUND RPMS in ${_mock_dir}  (package ${_name} ) .  Skipping...."
    fi
  

    # Tag with a "failed" file if the mock build failed to produce rpms:
    rpmcount=`ls -1 "${_mock_dir}"/*.rpm 2> /dev/null | wc -l`
    if [[ "$rpmcount" == "0" ]]; then
      touch ${_mock_dir}/failed
    fi	

  done
  ) &

done

# This script ends when all the mock commands finished - could be a while to build the whole distro(!)
wait
