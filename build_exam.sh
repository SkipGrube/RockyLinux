#!/bin/bash

# Used to be a simple build-success counting script, but now used to diagnose errors as well

# Our strategy: We want to go through each individual build results folder (that mock produced), and 
# see if there are RPMs in there.  If there are not, then the build failed.  In that case,
# we want to search for "No matching package" errors and figure out what we're missing to succeed.

# The idea is to take the output of this and copy/paste directly to the Wiki in markdown table.

# -Skip Grube, 2020-12-20


# EXAMPLE USE:
# Assume "BaseOS/" is a directory that contains folders named with RPM package/versions.
# Folders like: adcli-0.8.2-7.el8 , alsa-sof-firmware-1.5-2.el8 , arpwatch-2.1a15-44.el8 , at-3.1.20-11.el8 , etc.
# These folders contain mock logs and (if successful) built RPM artifacts
#
# cd ~/build/build_space/BaseOS/
# ~/src/RockyLinux/build_exam.sh | grep "BUILD OK"
# ~/src/RockyLinux/build_exam.sh | grep -v "BUILD OK"
# 
# The results are in Markdown table form and can be copied/pasted into the wiki


# Loop through folders in the current directory
# They are assumed to contain the build results, and be named according to package
for folder in `ls -1`; do
  

  # If there are RPM files in the build results directory, then the build is assumed to have succeeded, and we check the 
  # next folder for results
  rpmcount=`ls $folder/*.rpm 2> /dev/null   |   wc -l`
  

  # Short and dirty way to extract RPM name from it's full name-version string
  # (find the first instance of hyphen-number (like -1 , -2, etc.)
  _name=`echo "$folder" | sed 's/-[0-9].*//'`
  

  # If the build has RPMs produced, then we stop here and continue through the loop:
  if [[ "$rpmcount" != "0" ]]; then
    echo "| $_name | $folder | BUILD OK |"
    continue
  fi
	  
  

  # This long string of commands looks for the tell-tale "No matching package" error(s).
  # If found, we isolate the name(s) of the missing packages, make sure we don't repeat them, and store them in our REASON variable
  # (There is a hanging "\n" that gets swapped with a ",".  Too lazy to fix it for now)

  _tmp=`grep "No matching package to install" $folder/root.log`
  _tmp=`echo "$_tmp" | awk -F "install:" '{print $2}' | tr -d "\'" | sort | uniq | tr '\n' ','`
 
  
  # Check for the presence of a modular build error message.  If we find it, we know that this package will probably need MBS to build
  _tmp2=`grep 'filtered out by modular filtering' $folder/root.log | wc -l`
  

  # If we detected modular errors, then the fail reason is because of modularity
  # If the reason is just "," then the build failed for some other reason
  # Otherwise, our reason was due to the missing dependencies
  if [[ $_tmp2 -gt 0 ]]; then
    REASON="ERROR: Needs MBS or modularity enabled"
  elif [[ "$_tmp" == "," ]]; then
    REASON="ERROR: Unknown, needs investigation"
  else
    REASON="ERROR: Missing dependency: ${_tmp}"
  fi 
 

  # Output the package name and failure reason for easy markdown copy-paste to the Wiki
  echo "| $_name | $folder | $REASON |"
 
  

done
