Rocky Build Process Overview (Revised)
============

**UPDATE1: updated version of this document published 2020-12-14 , fixing MANY details**

This is a simple document that explains going from RHEL sources to freshly (re)-built Rocky Linux packages.  It's not intended to be overly technical or specific at all, just an introduction (and general plan) to someone interested in how the process will work.


## The Steps: Going from RHEL 8 Source to Rocky 8 Binary Package
1:  Obtain the RHEL sources via SRPM or CentOS Git 

2-3: Import RHEL source into Rocky Linux Git, replace any protected trademarks / branding from the source

4:  Produce a Rocky 8 Source RPM from the Rocky Linux Git repository for the package, likely using Koji/MBS/Mock RPM build tools

5: Compile the source RPM to a Rocky 8 binary RPM using the build tools

6: Sign and test the RPM in an automatic way

7: Deploy it to the Rocky Linux repository, and distribute to users

<br />

Obviously, each of these steps has a lot more to it.  This document will not get in-depth about ways and means of accomplishing each step.  We prefer each of these to be as automated as possible.  We'll take a (short) look at each one in turn, and the various options available to achieve them:

<br />

### Step 1: Obtain the Source
This is fairly straightforward.  If you want to re-build RHEL 8, you need the source to RHEL 8.  There are 2 main ways to do this:

* Download source RPM files on a RHEL machine via yum/dnf
* Copy them from https://git.centos.org (which are identical to RHEL and have tagged versions)
 
The consensus of the packaging team so far is to use the first method, importing SRPM files directly from upstream.  There are too many questions/uncertainties for the future in cloning the CentOS git repositories.

Fortunately, it's fairly easy to get the  via the ```reposync``` command.  The sources for a rebuild involve reposync'ing 4 repositories:

```
rhel-8-for-x86_64-baseos-source-rpm
rhel-8-for-x86_64-supplementary-source-rpms
rhel-8-for-x86_64-appstream-source-rpms
codeready-builder-for-rhel-8-x86_64-source-rpms
```

Downloading all of these repositories via yum/dnf should (in theory) give you all the source you need to re-create the entirety of RHEL 8.

Rocky Linux infrastructure should have 1-2 registered RHEL 8 machines who frequently check for new/updated packages from these source repositories.  New source RPM downloads can then be automatically copied/extracted in step 2.

<br />

### Steps 2-3: Import source to Git, and replace branding
This is a big technical question-mark, and will need to be carefully considered.

Each package in RHEL should have a corresponding Rocky Linux Git repository dedicated to it.  For example: Rocky Linux will have a bash repository, a python3 repository, a python3-gpg repository, etc.  **One git repo for each package.**  Yes, that is a lot of git repositories.

At time of this writing, the project is considering a self-hosted **Gitlab** instance, but that may change.

This section will outline some of the major technical hurdles being considered:


#### Second git that's private?
It is legally questionable to host raw RHEL material due to trademark issues.  There are a couple of options:

* Host a second, private git that holds raw RHEL sources waiting to be de-branded
* De-brand sources via script or patch as they are imported, and place the result into the main, public git repos

#### Git package strategy: source or binary?
Packages are distributed as specfiles, patches, and the upstream/original source as a tarball (.tar.gz, tar.bz2, etc.).  Should we store the binary tarballs in a separate connected repository with binary storage, or extract the raw source into our Git repositories?  Upstream CentOS/Fedora currently use the binary storage method, but are looking at raw source/text for the future.

* Binary/lookaside cache (via dist-git or git-lfs) to hold large tar files that contain sources (more complicated to understand)
* Upstream sources are extracted and placed directly in the Git repository (potentially very hard resource-wise on git cloning, bandwidth, and resource usage)


#### Files/Folders, Tags/Branches layout in Git:
Should we stick to the folders/tags/branches layout in git.centos.org?  Or something quite different?  Should we place debranding metadata with the project, or somewhere else?  How about automated/scripted test cases?  There is a lot to consider here.

<br />

### Step 4: Produce a Rocky Linux source RPM
Once in a Rocky repository, the contents of a package should correspond directly to its SRPM equivalent.

The debranding should be complete by this point, so a build system (Koji) will be able to point to the repository, grab the source for it, and construct our Rocky SRPM using Mock and other RPM tools.

Special attention will have to be paid to the "modular/stream" RPMs in RHEL 8.  The Modular Build System (MBS) add-on for Koji will almost certainly be required to account for this.

Particulars of the build config will depend on the answers to steps 2-3 above:  What folder(s) will things be in, where will the binary tar file data be located, etc.



<br />

### Step 5: Produce a Rocky Linux Binary RPM
This is pretty straightforward.  Once we have a valid source RPM, we use our build system to extract, compile, and produce a valid binary RPM.  

Again, special consideration is needed for modular/stream packages, and dependencies.

Once complete, we're ready to test, sign, and send it off to the official repository (and mirrors!).


<br />


### Step 6: Sign and Test
RPMs produced by us should be cryptographically signed with a Rocky Linux key, which guarantees to users that the package was indeed built by the Rocky Linux project.

The package will also need to be put through some testing - preferably automated.  The nature of the testing is yet to be determined, but we'll want to do some sanity checks at the bare minimum before unleashing it on the world.  (Is this package installable?  Did we accidentally miss any files?  Does it cause dnf/yum dependency conflicts? etc.)

<br />

### Step 7: Deploy to Repository
Once a package is complete and tested, it will be uploaded to the Rocky Linux repository, and picked up/cloned by a network of repository mirrors around the world.  The Rocky 8 source RPM will of course be uploaded as well.

Users of the distro will then see it when they `dnf update` or `dnf install` !


<br />

### Closing Note:
This is version "2" of this document.  We are now 6 days after the project was started.

The technical folks examining this have learned much in a short time, and already have working proof-of-concept RPM pipelines(!)  Come join in #rocky-devel-packaging on Slack and we're happy to discuss package/release pipeline direction.

There are still several questions to answer, particularly in regards to steps 2-3.  Progress is being made, though.

As said in v1 of this file, spelling out what needs to be done is much easier than actually accomplishing it.

This document remains a work-in-progress, as more technical information comes in.

This document remains a rough draft, and is likely to go through more revision as we learn more.

<br />

Thanks,

-Skip Grube (Slack)  (skip77 on IRC)
