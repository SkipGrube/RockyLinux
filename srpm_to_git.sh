#!/bin/bash

# Let's take an SRPM, extract the contents, and put it in a git repository

# This is what we'll do:
# 1. Extract SRPM contents to a tmp folder
# 2. Clone the target Git Repo containing SRPM sources
# 3. Remove contents of SOURCES/ and SPECS/ in the Git repo
# 4. Put *.spec from the SRPM -> SPECS/ in git, put all other SRPM sources in SOURCES/
# 5. Extract any source tarballs in SOURCES/ and remove the tar files.  Make metadata file with list of tar files we extracted
# 6. Execute ROCKY/debrand/rocky_debrand.sh -> should perform the debranding steps necessary for this project
# 7. Git -> check if any changes have been made.  Commit and push


# Things you will need:
# 1. Edit GIT_REMOTE with the location of your remote repos that hold the SRPM contents
# 2. We assume you have passwordless (ssh-key) access to the git repository
# 3. git, rpm, cpio, rpm2cpio installed


# Give the SRPM file as the first argument to this script
SRPM="$1"
GIT_REMOTE="$2"

if [ -z "${SRPM}" ] || [ -z "${GIT_REMOTE}" ]; then
  echo "ERROR: This script requires 2 arguments, a local SRPM file and a remote Git group that stores the package repositories.
Example:  ./srpm_to_git.sh  ./nginx-1.14.src.rpm   git@gitlab.com:SkipGrube/rockyrpms
"
exit 1
fi



# Get name and version of RPM
NAME=`rpm -qp --queryformat "%{NAME}" ${SRPM} 2> /dev/null`
VERSION=`rpm -qp --queryformat "%{VERSION}-%{RELEASE}"  ${SRPM} 2> /dev/null`


# Define our repository - combine git remote + name of project
GIT_URL="${GIT_REMOTE}/${NAME}.git"



########### Step 1: Extract SRPM contents
# Create temporary directory to extract our SRPM into
rm -rf _tmp
mkdir -p _tmp


# Extract SRPM to _tmp directory
rpm2cpio ${SRPM} | cpio -D _tmp/  -idmv


########## STEP 2 : Clone our Git source Repo
# Remove and re-clone source repo
rm -rf ${NAME}
git clone ${GIT_URL}



######### STEP 3: (temporarily) Wipe out existing SOURCES in the git repo.  We will re-extract from the SRPM:
rm -rf ${NAME}/SOURCES/*
rm -rf ${NAME}/SPECS/*



######## STEP 4: Put populate SOURCES/ and SPECS/ with contents from the SRPM
cd _tmp

# Record the spec file(s) and tarball names that we find
sourcetars="`ls -1 *.tar.*z*; ls -1 *.tar.*Z*`"
specs="`ls -1 *.spec`"

# put specs in the SPECS/ folder, and source code in the SOURCES/ folder
mv -f *.spec ../${NAME}/SPECS/
mv -f * ../${NAME}/SOURCES/



####### STEP 5: Loop through and extract any tarballs we found
# then remove the tarballs - we don't want them in git
cd ../${NAME}/SOURCES

for i in `echo "${sourcetars}"`; do
  # add code here to discover filetype, we're assuming .tar.gz for this mockup:
  tar --numeric-owner -xvzf  ${i}
  rm -f ${i}

done

# Add list of tar files to .rockymeta, so we know what to call them when they are turned into a Rocky SRPM
echo "${sourcetars}" > .rockymeta

cd ..



###### STEP 6:  REBRANDING: if we have a rocky_rebrand.sh script, it needs to be executed.  This will change any trademarks / notices from the upstream package before landing it in our git
if test -f "ROCKY/rebrand/rocky_rebrand.sh"; then
  (cd ROCKY/rebrand; ./rocky_rebrand.sh)
fi




###### STEP 7:  Check if anything changed, and commit to repo + tag if it did
# Now we are done - if something changed from the srpm, it should show up in the git status
# We should commit that

# If we pulled the exact same SRPM, and apply the exact same debranding, then no git changes should be present

if [ -n "$(git status -s)" ]; then
  git add ROCKY SOURCES SPECS
  git commit -m "Auto-committing for upstream version ${VERSION}"
  git tag -a "${VERSION}" -m "auto-build version ${VERSION}"
  git push
  git push --tags

else
  echo "NO Git changes were made, input SRPM appears identical to contents of ${GIT_URL}"
fi 

