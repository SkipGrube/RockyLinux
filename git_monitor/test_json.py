#!/usr/bin/env python3

import json
import pprint

import lib.setup as setup
from lib.tag_commit import *

#rocky_git_prefix="https://git.rockylinux.org/staging/"
#centos_git_prefix="https://git.centos.org/"

json_file="more_samples.json"

f = open(json_file, 'r')
json_data = f.readlines()
f.close()

pp = pprint.PrettyPrinter(indent=4)




for line in json_data:
  json_obj = json.loads(line)
  
  if 'tag' in json_obj:
    print("tag == " + str(json_obj['tag']))
    
    
  pp.pprint(json_obj)
  print("\n\n\n")





