#!/usr/bin/env python3

# Proposed csv / format for data:
# Timestamp-of-import  |  rpm_name  |  tag-version (end)  |  branch  |  Imported into Rocky?(yes/no)  |   In Rocky repository? (yes/no)  |  

import json, sys, os, time

# import our "setup" (global) variables and config:
import lib.setup as setup

# tag_commit class which holds data for each commit:
from lib.tag_commit import *
from lib.package_list import *


# tags_list is the main "master" list of objects which have our info
tags_list = []


json_file = "more_samples.json"
date = "2021-11-28_14:00"

# The program should be called with a list of files to "process": JSON data pulled from MQTT that contains commit info:
inFiles = sys.argv[1:]


def usageExit():
  print("Usage:  python3 script.py <List of Files>")
  sys.exit()
  


if (len(inFiles) == 0):
  print("ERROR: Need to be given at least 1 valid file with JSON commit data from MQTT to process!\n\n")
  usageExit()
  


for file in inFiles:
  if not os.path.exists(file):
    print("ERROR: One or more files in arguments doesn't seem to exist!\n\n")
    usageExit()
  
# Get a list of all source packages and their versions in the Rocky DNF repos
# It will be available as a dict lookup:  rocky_dnf.sourcePkg["pkgname"] = "name-version-release"
rocky_dnf = package_list()



for file in inFiles:
  
  # Open each file for reading, as we loop through them.  We assume each commit/tag time from upstream is the files modification time
  # (they should get rotated once per hour, which gives a good approximation)
  modTime = time.strftime('%Y-%m-%d %H:%M', time.localtime(os.path.getmtime(file)))
  
  f = open(file, 'r')
  json_data = f.readlines()
  f.close()
  
  for line in json_data:
    
    # Skip line if it's obviously too short to have json
    # (useful with hanging whitespace near end of the file)
    if len(line) < 6:
      continue
    
    #load the line into a temporary python dict
    json_tmp = json.loads(line.rstrip())
    
    # We are only looking for commits where a new version gets tagged
    # The branch MUST start with c8, but not be a c8s or a -beta branch, or we dont' care:
    if 'tag' in json_tmp:
      if '/c8' in json_tmp['tag'] and '/c8s' not in json_tmp['tag'] and '/c8-beta' not in json_tmp['tag']:
      #if '/c8' in json_tmp['tag'] and '/c8-beta' not in json_tmp['tag']:
        tags_list.append(tag_commit(modTime, json_tmp, rocky_dnf.sourcePkg))


# print header:
print("Time of Import  |  Package  |  Tagged version  |  Git Branch  |  Imported into Rocky?  |   In Rocky repository? ")

for i in range(0,len(tags_list)):
  #tags_list[i].debugMe()
  tags_list[i].printTagCommit()




