# Read all packages in the repos specified, and build a dictionary of pkg['name'] : "name-version-release"
#
# The objective is to be able to compare these versions from the Rocky repository against the incoming tags/releases from git.centos.org

import subprocess, re, sys

import lib.setup as setup

class package_list:

  # Dictionary that holds package name and "sanitized" release tag (module and el# info removed)
  sourcePkg = {}
  
  def __init__(self):
    self.sourcePkg = {}
    
    # Run command, get output, and translate into string:
    dnf_cmd = subprocess.Popen('dnf repoquery --repo "' + str(setup.DNF_REPOS) + '"  --latest-limit 1  --queryformat  "%{name} %{name}-%{version}-%{release}"', shell=True, stdout=subprocess.PIPE)
    pkg_result = dnf_cmd.stdout.read().decode('utf-8').split("\n")


    # Import each line of the repoquery output into our dictionary:  sourcePkg['package_name'] = 'name-version-release'
    for pkg in pkg_result:
      
      # Take the package version and remove the "+###+###+###" tag from modular packages, and the ".el#" tag from normal packages
      # We do the same for the RHEL Git tags, so the version comparison is valid!      
      pkg = re.sub('(.+\.module)\+(el\d+\.\d+\.\d+)\+(\d+)\+([A-Za-z0-9]{8})(\..+)?', '\\1\\5', pkg)
      pkg = re.sub('(\.el\d+(?:_\d+|))', '', pkg)
      
      
      # Actually populate the dictionary, with ['name'] == "name-version-release" (from our dnf repoquery output)
      if " " in pkg: 
        p_line = pkg.split(" ")
        self.sourcePkg[str(p_line[0])] = str(p_line[1])
      
      
    
    
  
