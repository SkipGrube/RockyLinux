# global variables that the git_monitor program uses:


# Prefixes are the URL before "rpms/RPMNAME"
ROCKY_PREFIX = "https://git.rockylinux.org/staging/"

UPSTREAM_PREFIX = "https://git.centos.org/"

# Comma-separated list (no spaces) of Rocky DNF repos to query against
# (to match RHEL import tags <----> Rocky packages
DNF_REPOS="baseos-source,appstream-source,powertools-source"

