#!/bin/bash

# Simple script to read list of packages from "dnf module info" and figure out where they go in the build order.
#
# We can use this information to figure out when we should build a module
# Eg. Every python38 package completed with or before "Build Pass 3", therefore you should build module python38 with build pass 3

# output file with the results of dnf module info <modulename>:
TXTFILE=$1

BUILDRESULTS=/var/www/html/RockyDevel/deliverables

IFS='
'

# Loop through each package in the module stream list (excluding src , debugsource, and debuginfo)
for pkgname in `grep "module_el8" $TXTFILE  |  grep -v '\.src'  | grep -v 'debuginfo'  | grep -v 'debugsource'  |  sed 's/\.x86_64//' | sed 's/\.noarch//'  |  sed 's/\.i686//' | awk '{print $NF}'`; do 
  
  _result=0
  #_pkg=`echo "$i" | awk '{print $NF}'`
  #echo "$i"
 


  # Loop through each build_pass folder under the deliverables/ directory (the one that contains our rpm lists)
  # and look for our package
  for build in `ls -1 $BUILDRESULTS  | grep "build_pass" |  sort -V`; do
    
    # isolate package version to match with results:
    _pkg=`echo "$pkgname" | awk -F ':' '{print $NF}'`


    _result=`grep "$_pkg" $BUILDRESULTS/$build/*success.txt 2> /dev/null | wc -l `
    #echo "DEBUG :: found $_result  matches looking for $pkgname"
    
    
    if [[ "$_result" -gt "0" ]]; then
      break
    fi
    
    
  done



  if [[ "$_result" -gt "0" ]]; then
    echo -e "$pkgname \t\t $build"
  else
    echo -e "$pkgname \t\t NOT_FOUND"
  fi

done


