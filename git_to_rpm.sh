#!/bin/bash

# Retrieve the contents of a Rocky Linux git repository and build a source RPM package from it

# Takes a lot of arguments :)
# (ex: git@gitlab.com:SkipGrube/rockyrpms/nginx.git )
#

# This script assumes:
# -mock build tool is installed, and this user is in the mock group
# -passwordless/key access to the remote git repo specified
# -git, tar, curl and other common core commands (mkdir, etc.) are available

# Heavily patched by Rickard Osser <rickard.osser@bluapp.com>

# Order of operations depending on selected options:
# 
# 1. Clone git repository
# 2. compress upstream source code inside SOURCES/ back into tar.gz files
# 3. copy specs and SOURCES/ into temporary directory
# 4. call mock build tool to produce an SRPM
# 5. call mock build tool to compile the SRPM and produce the RPMS

function usage(){
	    echo "Usage: git_to_rpm [OPTION...] <URL>
Fetch the package source from a git or SRPM and compile the package.
		 -c	compile the resulting or fetched SRPM
	    	 -b 	select git branch
		 -d	comma separated defines ex. "with_dc 1" for compiling the SRPM
		 -r	comma separated paths to custom repos (local or online) for compiling SRPM
		 -h	display this help and exit
		 <URL>	Either a git-repo https://git.centos.org/rpms/at.git or a SRPM URL"
}

COMPILE=0
ADDREPO=""
DEFINES=""

while getopts ":b:d:r:ch" option; do
    case ${option} in
	b ) #For option b (git Branch)
	    #echo "Option fetch branch: $OPTARG"
	    GITBRANCH=$OPTARG
	    ;;	
	d ) #For option d (rpmbuild defines)
	    #echo "Option fetch branch: $OPTARG"
	    IFS=', ' read -r -a array <<< "$OPTARG"
	    
	    for DEFINE in "${array[@]}"
	    do
		DEFINES="$DEFINES --define $DEFINE "
	    done
	    ;;
	c ) #For option c (Compile)
	    #echo "Option: Compile"
	    COMPILE=1
	    ;;
	r ) #For option r (Custom repo)
	    #echo "Option custom repo: $OPTARG"
	    IFS=', ' read -r -a array <<< "$OPTARG"

	    for REPO in "${array[@]}"
	    do
		ADDREPO="$ADDREPO --addrepo $REPOS "
	    done
	    #echo "$ADDREPO"
	    ;;
	h ) #For option h Help
	    usage
	    exit 0
	    ;;
        *|\? )
	    echo "Unimplemented option: -$OPTARG" 1>&2
	    usage
	    exit 1
	    ;;
        :  )
	    echo "Error: $OPTARG requires an argument" 1>&2
	    usage
	    exit 1
	    ;;
    esac
done
if ((OPTIND == 1))
then
    echo "No options specified"
fi

shift $((OPTIND - 1))

if (($# == 0))
then
    echo "Error: No URL specified"
    exit 1;
else
    URL=$1
    FILENAME=`echo $URL | xargs basename`
    if [[ `echo $FILENAME | grep src.rpm` != "" ]]; then
	SRPM=$URL
    else
	GITREPO=$URL
    fi
fi

###### We are doing a GIT a SRPM

if [ $GITREPO ]; then
    ###### Step 1: clone git repo (and record project name)
    
    # Extract name of project by pulling end of gitrepo URL
    # Spectool might do this more reliably
    NAME=`echo "${GITREPO}" | awk -F '/' '{print $NF}' | sed 's/\.git//'`
    name=${NAME}
    VERSION=`grep "Version:" ${NAME}/SPECS/*.spec | awk -F " " '{ print $2 }'`
    version=${VERSION}
    
    # Clone it
    rm -rf ${NAME}
    if [ -n "${GITBRANCH}" ]; then
	GITOPTION=" -b ${GITBRANCH}"
    fi
    git clone ${GITREPO} $GITOPTION
    
    echo "${NAME}-${VERSION}.tar.gz" > "${NAME}/SOURCES/.rockymeta"
    
    ###### Step 2: Compress upstream sources folder(s) back into tarballs
    
    cd ${NAME}/SOURCES
    
    # .rockymeta file contains list of tarballs that were part of the original upstream SRPM
    for i in `cat .rockymeta`; do
	
	# We get the proper folder name by the name of the tar minus the filename extensions
	folder=`echo "${i}"   |   awk -F '.tar' '{print $1}'`
	
	# Right now just assume .tar.gz.  Obviously will need to support the different compressions
	# (.tar.bz2, .tar.xz, .tar.Z , etc.)
	#touch ${folder}.tar.asc
    done

    ########## SPECIAL PATCH: This is to change the spec-file of the SIG samba to compile AD DC... Not a nice place to put it... I know /Ricky
    awk -F '/^Source/|#' '{ print $1 }' ../SPECS/*.spec | sed 's/^xzcat/zcat/g' | sed 's/%global with_dc 0/%global with_dc 1/g' > ../SPECS/*.spec.new
    mv ../SPECS/*.spec.new ../SPECS/*.spec
    
    
    # Back to CWD, outside of git root
    cd ../../
    
    ##### Step 3: copy SPECS + SOURCES into temporary location
    # temporary build location
    rm -rf tmp
    mkdir tmp
    
    # Get sources (including tarballs) into temporary build location
    cp -rf ${NAME}/SOURCES/* tmp/
    cp -rf ${NAME}/SPECS/*  tmp/
    


    ##### Step 4: Build SRPM  (via mock)
    rm -rf srpm
    mkdir -p srpm

    # ID .spec file for project (should be only 1):
    specfile=`ls -1 tmp/*.spec | head -1`

    # Actually build srpms:
    mock --enable-network --buildsrpm --sources tmp/ --define "_disable_source_fetch 0" --spec ${specfile} --resultdir=./srpm
    
    echo "COMPLETE ::  SRPM for ${NAME} is located in ./srpm/ "
else
    rm -rf srpm
    mkdir -p srpm
    curl -k -o srpm/$FILENAME $SRPM  1>&2
    NAME=`rpm -qil srpm/$FILENAME  | grep Name | awk -F ":" '{ print $2 }' |  tr -d '[:space:]'` 1>&2
    
    echo "COMPLETE ::  SRPM for ${NAME} is located in ./srpm/ "
fi
    

if [[ $COMPILE -eq 1 ]]; then    
    # Actually build rpms:
    rm -rf rpm
    mkdir -p rpm
    
    # SAMBA REPO DEPENDENCY: http://www.cert.org/forensics/repository/centos/cert/8/x86_64
    mock srpm/${NAME}*src.rpm ${DEFINES} ${ADDREPO} --resultdir=./rpm
    
    echo "COMPLETE ::  RPM for ${NAME} is located in ./rpm/ "

fi

exit 0

